FROM node:17.6-alpine3.14
WORKDIR /usr/app
COPY package*.json ./
RUN npm install --force
COPY . ./
CMD ["npm","run","dev"]