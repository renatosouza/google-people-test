# Integração com Google People API

Projeto de testes para integração com [Google People API](https://developers.google.com/people/api/rest/v1/people).

## Para iniciar a aplicação localmente:

crie um arquivo .env na raiz do projeto conforme o modelo abaixo:

```.env
CLIENT_ID=<suas-credenciais>
CLIENT_SECRET=<suas-credenciais>
LOG_LEVEL=development
DB_USER=postgres
DB_PASS=<senha>
DB=<nome-banco>
DB_DIALECT=postgres
DB_HOST=<host-banco>
DB_PORT=<porta-banco>
PORT=<porta-padrão-applicação>
```

## **Atenção:**

**Client_id e Client_secret são obtidos através da configuração de um app no Google Cloud, para este projeto é habilitar o consumo da PeopleAPI e a documentação do processo segue abaixo.**

**Caso deseje alterar a senha do banco de dados local, basta mudar a variável de ambiente correspondente no docker-compose e colocar o mesmo valor no seu aruivo `.env` em `DB_PASS`**

[People API Docs](https://developers.google.com/people/v1/getting-started)

Para iniciar o projeto é necessário um banco de dados Postgres rodando local ou remotamente, como preferir. Basta informar os dados de acesso no arquivo [.env](#para-iniciar-a-aplicação-localmente)

Para facilitar o trabalho de start da aplicação em ambiente local, há um `Dockerfile` e um `docker-compose` evitando a necessidade de instalação de um banco local ou provisionamento em outro ambiente para acesso remoto.

## Para rodar localmente por container:

Faça o build da imagem Docker com o comando:

**É importante que este comando seja inicializado na pasta raiz do projeto onde se encontra o arquivo Dockerfile, caso deseje utilizá-lo em outro local, se faz necessário apontar o arquivo Docker desejado com a flag `-f path_dockerfile`.**

`docker build -t google-people-integration .`

Em seguida, para iniciar a aplicação e banco de dados:

`docker compose up`

O console deve mostrar a seguinte informação:

```log
[2022-22-03, 12:42:18 PM] INFO (92 on 2a32b726ddbc): google-people-test listening on port: 8080
```

## Fluxo

O fluxo compreende é bem simples e compreende apenas duas chamadas. Uma manual e que deve ser feita em um browser `/v1/auth` e outra que ocorre automaticamente como callback do fluxo de autenticação da Google `/v1/google-connector`. Após este passo é retornado o usuário cadastrado no banco de dados com as informações obtidas.

## Endpoints:

Auth

Redirect para Goolge authentication.

```
GET /v1/auth
```

Callback da autenticação.

```
GET /v1/google-connector
```

## Retorno:

O retorno é no formato abaixo:

```json
{
  "id": "12093741234810200012",
  "firstName": "Fulano",
  "lastName": "da Silva",
  "email": "fulano@silva.com",
  "profilePicture": "https://imgur.com/BrW201S.png"
}
```
