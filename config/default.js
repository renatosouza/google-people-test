require('dotenv').config();

module.exports = {
  Logger: {
    level: process.env.LOG_LEVEL || 'development',
  },
  Server: {
    Port: process.env.PORT || 8080,
  },
  Google: {
    clientId: process.env.CLIENT_ID,
    clientSecret: process.env.CLIENT_SECRET,
    redirectUrl: `http://127.0.0.1:${process.env.PORT || 8080}/v1/google-connector`,
  },
  Database: {
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: process.env.DB_DIALECT,
    logging: false,
    define: {
      timestamps: true,
    },
  },
};
