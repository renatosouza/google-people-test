const { generateLoginURL } = require('./googleapis-service');

module.exports = {
  generateLoginRedirectURL(config = {}) {
    return generateLoginURL(config);
  },
};
