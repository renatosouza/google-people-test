const config = require('config');
const { google } = require('googleapis');

const DEFAULT_SCOPES = [
  'https://www.googleapis.com/auth/userinfo.email',
  'https://www.googleapis.com/auth/userinfo.profile',
];

const oauth2Client = new google.auth.OAuth2(
  config.get('Google.clientId'),
  config.get('Google.clientSecret'),
  config.get('Google.redirectUrl')
);

google.options({ auth: oauth2Client });

const generateLoginURL = ({ params = { access_type: 'offline' }, scopes = DEFAULT_SCOPES }) => {
  const authConfig = {
    ...params,
    scope: scopes,
  };
  return oauth2Client.generateAuthUrl(authConfig);
};

module.exports = { google, generateLoginURL, oauth2Client };
