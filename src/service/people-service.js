const { google } = require('./googleapis-service');
const logger = require('../lib/logger');

const people = google.people('v1');

const DEFAULT_USER_FIELDS = ['names', 'emailAddresses', 'photos'];

function getPersonData(fields = DEFAULT_USER_FIELDS) {
  if (!Array.isArray(fields) && fields.length === 0) {
    logger.warn(
      `Invalid user fields information requested, the default fields will be sent. \n REQUESTED: ${fields} \n DEFAULTS: ${DEFAULT_USER_FIELDS.join()}`
    );
    fields = DEFAULT_USER_FIELDS;
  }
  return people.people.get({
    resourceName: 'people/me',
    personFields: fields.join(),
  });
}

module.exports = { getPersonData };
