const User = require('../database/models/user');

async function save(user) {
  return User.create({ ...user });
}

module.exports = {
  save,
};
