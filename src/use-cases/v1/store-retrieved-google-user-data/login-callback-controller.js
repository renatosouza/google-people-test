const { parseUserData } = require('./google-user-parser');
const { getPersonData } = require('../../../service/people-service');
const { oauth2Client } = require('../../../service/googleapis-service');
const UserService = require('../../../service/user-service');
const logger = require('../../../lib/logger');

const router = require('express').Router();

router.get('/google-connector', async (req, res) => {
  const authorizationCode = req.query.code;

  if (authorizationCode) {
    const { tokens } = await oauth2Client.getToken(authorizationCode);
    oauth2Client.setCredentials(tokens);

    try {
      const peopleData = await getPersonData();
      const user = await UserService.save(parseUserData(peopleData.data));
      return res.status(200).json(user).end();
    } catch (error) {
      logger.error(error);
      return res.status(500).send();
    }
  }

  return res.status(400).json({ error: 'no authorization code!' });
});

module.exports = router;
