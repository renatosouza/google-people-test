function parseUserData(data) {
  const id = data.names.length > 0 ? data.names[0].metadata?.source?.id : 0;
  const firstName = data.names.length > 0 ? data.names[0].givenName : '';
  const lastName = data.names.length > 0 ? data.names[0].familyName : '';
  const profilePicture = data.photos.length > 0 ? data.photos[0].url : '';
  const email = data.emailAddresses.length > 0 ? data.emailAddresses[0].value : '';

  return { id: parseInt(id), firstName, lastName, profilePicture, email };
}

module.exports = { parseUserData };
