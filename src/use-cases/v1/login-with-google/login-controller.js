const { generateLoginRedirectURL } = require('../../../service/login-service');

const router = require('express').Router();

router.get('/auth', (_, response) => {
  const redirectURL = generateLoginRedirectURL();
  if (redirectURL) {
    return response.redirect(redirectURL);
  }
  return response
    .status(500)
    .json({ error: 'Expected redirectURL to Login provider was not generated.' })
    .end();
});

module.exports = router;
