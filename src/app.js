require('dotenv').config();
require('./database');

const config = require('config');
const Server = require('./server');

const server = new Server(config.get('Server.Port'));
server.config();
server.start();
