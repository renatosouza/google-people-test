const { Sequelize } = require('sequelize');
const config = require('./database');

class Database {
  constructor() {
    this.init();
  }
  init() {
    this.connection = new Sequelize(config);
    this.connection.sync({ force: true });
  }
}

module.exports = new Database();
