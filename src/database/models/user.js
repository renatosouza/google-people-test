const config = require('config');
const { Sequelize, DataTypes } = require('sequelize');
const sequelize = new Sequelize(config.get('Database'));

const User = sequelize.define('User', {
  id: {
    type: DataTypes.STRING,
    primaryKey: true,
  },
  createdAt: {
    type: DataTypes.DATE,
    defaultValue: sequelize.NOW,
  },
  updatedAt: {
    type: DataTypes.DATE,
    defaultValue: sequelize.NOW,
  },
  firstName: DataTypes.STRING,
  lastName: DataTypes.STRING,
  email: DataTypes.STRING,
  profilePicture: DataTypes.STRING,
});

module.exports = User;
