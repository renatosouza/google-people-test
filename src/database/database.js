const config = require('config');

const databaseConfig = config.get('Database');
module.exports = { ...databaseConfig };
