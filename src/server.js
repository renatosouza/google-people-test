const config = require('config');
const express = require('express');
const cors = require('cors');
const helmet = require('helmet');

const AppInfo = require('../package.json');
const logger = require('./lib/logger');
const LoginCallbackController = require('./use-cases/v1/store-retrieved-google-user-data/login-callback-controller');
const GoolgeLoginController = require('./use-cases/v1/login-with-google/login-controller');

class Server {
  constructor(port = config.get('Server.Port')) {
    this.port = port;
    this.app = express();
  }

  config() {
    this.app.use(cors());
    this.app.use(helmet());

    const v1 = express.Router();
    v1.use(GoolgeLoginController);
    v1.use(LoginCallbackController);

    this.app.use('/v1', v1);
  }

  start() {
    this.app.listen(this.port, () => {
      logger.info(`${AppInfo.name} listening on port: ${this.port}`);
    });
  }
}

module.exports = Server;
