const { default: pino } = require('pino');
const path = require('path');
const AppConfig = require('../../package.json');

const logger = pino(
  {
    transport: {
      target: 'pino-pretty',
      options: {
        colorize: true,
        translateTime: 'yyyy-dd-mm, h:MM:ss TT',
      },
    },
  },
  pino.destination(`${path.resolve('logs', AppConfig.name, AppConfig.name + '.log')}`)
);

module.exports = logger;
